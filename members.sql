-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 17, 2020 at 09:43 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `urhope_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `regno` varchar(100) DEFAULT NULL,
  `phone` char(11) DEFAULT NULL,
  `pin` char(6) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `age` varchar(100) DEFAULT NULL,
  `sex` varchar(100) DEFAULT NULL,
  `currProfile` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `social` varchar(100) DEFAULT NULL,
  `services` varchar(100) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `govtID` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

CREATE TABLE `task` (
   `id` int(11) not null auto_increment,
   `task` varchar(100),
   `grp` varchar(100),
   `website` varchar(100),
   `t_type` varchar(100),
   `phone` char(11),
   `pin` char(6),
   `vol_num` varchar(100),
   `task_det` varchar(255),
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

--- Table 

INSERT INTO `members` (`id`, `name`, `username`, `password`, `regno`, `phone`, `pin`, `role`, `website`, `age`, `sex`, `currProfile`, `address`, `social`, `services`, `branch`, `about`, `govtID`) VALUES
(1, 'Zuhair Abbas', 'zuhairabs@gmail.com', 'bf5bd1eb9ec20084c050fe41cd341d39', NULL, '9022122553', '400055', 'v', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Kisan Upliftment', 'contact@kisan.com', '4b30e136ad8a2bec2ba41fbb536f244d', NULL, '838274376', '110006', 'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
